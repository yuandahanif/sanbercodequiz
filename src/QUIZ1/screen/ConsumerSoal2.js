import React, {useContext} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';

import {RootContext} from '../providers/RootContext';

export default function ConsumerSoal2() {
  const {trainers, addTrainers} = useContext(RootContext);

  return (
    <View style={styles.container}>
      <FlatList
        data={trainers}
        contentContainerStyle={styles.container}
        renderItem={({item}) => (
          <View>
            <Text>{item.name}</Text>
            <Text>{item.position}</Text>
          </View>
        )}
        keyExtractor={(data) => data.name}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
  },
});
