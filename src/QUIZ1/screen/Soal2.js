import React, {useState} from 'react';
import {RootContext} from '../providers/RootContext';
import ConsumerSoal2 from './ConsumerSoal2';

// Buatlah sebuah Consumer untuk menampilkan data kedalam FlatList dari Provider Context API berikut ini
const ContextAPI = () => {
  const [trainers, setTrainers] = useState([
    {
      name: 'Zakky Muhammad Fajar',
      position: 'Trainer 1 React Native Lanjutan',
    },
    {
      name: 'Mukhlis Hanafi',
      position: 'Trainer 2 React Native Lanjutan',
    },
  ]);

  const addTrainers = (trainer) => {
    setTrainers((prevState) => {
      return [...prevState, trainer];
    });
  };

  return (
    <RootContext.Provider value={{trainers, addTrainers}}>
      <ConsumerSoal2 />
    </RootContext.Provider>
  );
};

export default ContextAPI;
