import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';

// Rubahlah component berikut ini menjadi sebuah Function component
export default function FComponent() {
  const [name, setName] = useState('Jhon Doe');

  useEffect(() => {
    setTimeout(() => {
      setName('Asep');
    }, 3000);
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{name}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 16,
        textAlign: 'center',
    }
})
